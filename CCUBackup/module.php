<?php

// Klassendefinition
class CCUBackup extends IPSModule
{

    // Überschreibt die interne IPS_Create($id) Funktion
    public function Create()
    {

        // Diese Zeile nicht löschen.
        parent::Create();

        //Attribute registrieren
        $this->RegisterAttributeString('SessionID', '');
        $this->RegisterAttributeString('BackupFile', '');

        //Variablen registrieren
        $this->RegisterPropertyString('BackupPath', '');
        $this->RegisterPropertyInteger('BackupDays', 7);
        $this->RegisterPropertyInteger('BackupIntervall', 0);
        $this->RegisterPropertyString('BackupTime', '{"hour":4, "minute": 44, "second": 44}');
        $this->RegisterPropertyString('CCUAddress', '');
        $this->RegisterPropertyString('Username', 'Admin');
        $this->RegisterPropertyString('Password', '');
        $this->RegisterPropertyBoolean('SSL', false);
        $this->RegisterPropertyBoolean('Debug', false);
        $this->RegisterPropertyBoolean('SizeCheck', false);

        //Profil erstellen
        if (!IPS_VariableProfileExists('CCUB.State')) {
            IPS_CreateVariableProfile('CCUB.State', 1);
            IPS_SetVariableProfileAssociation('CCUB.State', 0, $this->Translate('failed'), '', 0xFF0000);
            IPS_SetVariableProfileAssociation('CCUB.State', 1, $this->Translate('success'), '', 0x00FF00);
        }

        //Variablen erstellen
        if (!@$this->GetIDForIdent('STATE')) {
            $this->RegisterVariableInteger('STATE', $this->Translate('Result'), 'CCUB.State', 1);
        }
        if (!@$this->GetIDForIdent('LAST')) {
            $this->RegisterVariableInteger('LAST', $this->Translate('Latest backup'), '~UnixTimestamp', 2);
        }

        //Benachrichtigung
        $this->RegisterPropertyInteger('SmtpVarEnabled', 0);
        $this->RegisterPropertyInteger('SmtpInstanceID', 0);
        $this->RegisterPropertyBoolean('SmtpMsgEnabled', false);
        $this->RegisterPropertyBoolean('SmtpMsgOnlyError', false);

        //Timer registrieren
        $this->RegisterTimer('CCU_Backup', 0, 'CCUB_Start_Backup($_IPS[\'TARGET\']);');
    }

    // Überschreibt die intere IPS_ApplyChanges($id) Funktion
    public function ApplyChanges()
    {
        // Diese Zeile nicht löschen
        parent::ApplyChanges();

        //Register Kernel Messages
        $this->RegisterMessage(0, IPS_KERNELSTARTED);

        //IP-Symcon Kernel bereit?
        if (IPS_GetKernelRunlevel() !== KR_READY) {
            $this->SendDebug(__FUNCTION__, $this->Translate('Kernel is not ready! Kernel Runlevel = ') . IPS_GetKernelRunlevel(), 0);
            return false;
        }

        //Check Path
        $Result = $this->Check_BackupPath();
        if ($Result > 102) {
            $this->SetStatus($Result);
            return false;
        }

        //Instanzen OK?
        if ($this->CheckInstance() === false) {
            return false;
        }

        //Timer-Intervall setzen
        $this->SetMyTimerInterval();

        //Check OK
        $this->SetStatus(102);
        return true;
    }

    public function Start_Backup()
    {
        //Check Path
        $Result = $this->Check_BackupPath();
        $this->SetStatus($Result);
        if ($Result > 102) {
            $this->myDebug(__FUNCTION__ . '()', 'Error in the backup path, check the module settings!');
            $this->SendMessages(false, 'Symcon-CCUBackup: ' . $this->Translate('failed'), $this->Translate('Error in the backup path, check the module settings!'));
            $this->Fill_Variables(false, time());
            return false;
        }

        //Login
        $Result = $this->Check_Login();
        if ($Result === false) {
            $this->myDebug(__FUNCTION__ . '()', 'Login error');
            $this->SendMessages(false, 'Symcon-CCUBackup: ' . $this->Translate('failed'), $this->Translate('login error'));
            return false;
        }

        //Backup
        $this->myDebug(__FUNCTION__ . '()', 'Wait for CCU backupfile..');
        $result = $this->CURL('/config/cp_security.cgi?sid=@' . $this->ReadAttributeString('SessionID') . '@&action=create_backup', '', true);
        $result = json_decode($result, true);
        //echo var_dump($result);

        $BackupFile = $this->ReadAttributeString('BackupFile');
        if (@$result['result'] === true || @$result['error'] === null) {
            if (filesize($this->ReadAttributeString('BackupFile')) > 50000) { //Check Filesize!
                $this->WriteAttributeString('SessionID', '');
                $this->Fill_Variables(true, time());
                $myReturn = true;
                $this->SendMessages(true, 'Symcon-CCUBackup: ' . $this->Translate('success'), $this->Translate('Backup path') . ': ' . $BackupFile);
            } else {
                $this->myDebug(__FUNCTION__ . '()', 'ERROR: File size to small: ' . filesize($BackupFile) . ' byte');
                $myReturn = false;
            }
        } else {
            $this->Fill_Variables(false, time());
            $myReturn = false;
            $this->SendMessages(false, 'Symcon-CCUBackup: ' . $this->Translate('failed'), $this->Translate('Backup path') . ': ' . $BackupFile);
        }

        //Logoff
        $Result = $this->Check_Logoff();
        if ($Result === false) {
            $this->myDebug(__FUNCTION__ . '()', 'logout error');
            $this->SendMessages(false, 'Symcon-CCUBackup: ' . $this->Translate('failed'), $this->Translate('logout error'));
            return false;
        }

        //Delete Old Backups
        if ($myReturn === true && $this->ReadPropertyInteger('BackupDays') > 0) {
            $this->DeleteOldBackups();
        }

        //Timer-Intervall setzen
        $this->SetMyTimerInterval();

        return $myReturn;
    }

    public function Form_Login()
    {
        if ($this->ReadAttributeString('SessionID') !== '') {
            echo $this->Translate('You are already logged in') . ', SessionID: ' . $this->ReadAttributeString('SessionID');
            return;
        }
        if ($this->Check_Login() === true) {
            echo $this->Translate('You are now logged in') . ', SessionID=' . $this->ReadAttributeString('SessionID');
        } else {
            echo $this->Translate('Login failed! Login data correct?');
        }
    }

    public function Form_Registrationstatus()
    {
        $this->myDebug(__FUNCTION__ . '()', '');
        if ($this->ReadAttributeString('SessionID') === '') {
            echo $this->Translate('Logged out');
        } else {
            echo $this->Translate('Logged in') . ', SessionID=' . $this->ReadAttributeString('SessionID');
        }
    }

    public function Form_Logoff()
    {
        $this->myDebug(__FUNCTION__ . '()', 'Start logoff');
        if ($this->ReadAttributeString('SessionID') === '') {
            echo $this->Translate('Logged out');
            return;
        }
        if ($this->Check_Logoff() === true) {
            echo $this->Translate('You are now logged out');
        } else {
            echo $this->Translate('You are now logged out');
        }
    }

    public function Form_FreeSpace() {
        $Path = $this->ReadPropertyString('BackupPath');
        $Free = disk_free_space ( $Path );
        echo $this->ReadableSize($Free);
    }

    public function GetConfigurationForm()
    {
        $InfoArray = $this->ModulInfo();
        $Name = $InfoArray['Name'];
        $Version = $InfoArray['Version'];
        $Build = $InfoArray['Build'];
        return '{
            "elements":
                [
                    {"type": "Label", "caption": "' . $Name . ' - v' . $Version . ' (Build: ' . $Build . ') © by SimonS"},        
                    {"type": "Label", "caption": "_________________________________________________________________________________________________________________________________________________________________________" },
                    {"type": "RowLayout", "items": [
                        {"type": "ValidationTextBox", "name": "CCUAddress", "caption": "IP or DNS (Optional port)", "validate": "^(([a-z0-9]|[a-z0-9][a-z0-9\\\-]*[a-z0-9])\\\.)*([a-z0-9]|[a-z0-9][a-z0-9\\\-]*[a-z0-9])(:[0-9]+)?$" },
                        {"type": "Label", "caption": "          " },
                        {"type": "ValidationTextBox","name": "Username","caption": "Username"},
                        {"type": "Label", "caption": "          " },
                        {"type": "PasswordTextBox", "name": "Password", "caption": "Password" }
                    ] },
                    {"type": "CheckBox", "name": "SSL", "caption": "Use SSL (Available from CCU3)" },
                    {"type": "Label", "caption": "_________________________________________________________________________________________________________________________________________________________________________" },
                    {"type": "Label", "caption": "Backup:" },
                    {"type": "RowLayout", "items": [
                        {"type": "ValidationTextBox","name": "BackupPath","caption": "Backup path"},
                        {"type": "Label", "caption": "          " },
                        {"type": "NumberSpinner", "name": "BackupDays", "caption": "Delete Backups older x days", "minimum": 0, "maximum": 365 },
                        {"type": "Label", "caption": "          " },
                        {"type": "CheckBox", "name": "SizeCheck", "caption": "Check free disk space" }                        
                    ] },
                    {"type": "Label", "caption": "_________________________________________________________________________________________________________________________________________________________________________" },
                    {"type": "Label", "caption": "Timer: (0 = deactivated)" },
                    {"type": "RowLayout", "items": [
                        {"type": "NumberSpinner", "name": "BackupIntervall", "caption": "Automatic backup every x days:", "minimum": 0, "maximum": 31 },
                        {"type": "Label", "caption": "          " },
                        {"type": "SelectTime", "name": "BackupTime", "caption": "Time"}
                    ] },                   
                    {"type": "Label", "caption": "_________________________________________________________________________________________________________________________________________________________________________" },
                    {"type": "Label", "caption": "Notification:" },
                    {"type": "RowLayout", "items": [
                        {"type": "SelectInstance", "name": "SmtpInstanceID", "caption": "SMTP instance" },
                        {"type": "Label", "caption": "          " },
                        {"type": "SelectVariable", "name": "SmtpVarEnabled", "caption": "Boolean variable (optional on/off switch)" },
                        {"type": "Label", "caption": "          " },
                        {"type": "CheckBox", "name": "SmtpMsgEnabled", "caption": "Mail notification" },
                        {"type": "Label", "caption": "          " },
                        {"type": "CheckBox", "name": "SmtpMsgOnlyError", "caption": "Only on error" }
                    ] },
                    {"type": "Label", "caption": "_________________________________________________________________________________________________________________________________________________________________________" },
                    {"type": "CheckBox", "name": "Debug", "caption": "Debug" }
                ],
                "actions":
                [
                    {"type": "Label", "caption": "CCU Anmeldung testen"},
                    {"type": "RowLayout", "items": [
                        {"type": "Button", "caption": "Login", "onClick": "CCUB_Form_Login($id);" },
                        {"type": "Button", "caption": "Registrationstatus", "onClick": "CCUB_Form_Registrationstatus($id);" },
                        {"type": "Button", "caption": "Logoff", "onClick": "CCUB_Form_Logoff($id);" }
                    ] },
                    {"type": "Label", "caption": "Backup"},
                    {"type": "RowLayout", "items": [
                        {"type": "Button", "caption": "Start", "onClick": "CCUB_Start_Backup($id);" }
                    ] },
                    {"type": "Label", "caption": "Documentation"},
                    {"type": "RowLayout", "items": [
                        {"type": "Button", "caption": "Website", "onClick": "echo \'https://ips.air5.net/\';" },
                        {"type": "Button", "caption": "Documentation", "onClick": "echo \'https://gitlab.com/sg-ips-module/public/CCUBackup/-/blob/master/README.md\';" }
                    ] }
                ],
                "status":
                [
                    {"code": 102, "icon": "active", "caption": "CCUBackup module is active" },
                    {"code": 201, "icon": "error", "caption": "ERROR: Backup path must not be empty!" },
                    {"code": 202, "icon": "error", "caption": "ERROR: Backup path does not exist!" },
                    {"code": 203, "icon": "error", "caption": "ERROR: Backup path is not writable, check write permissions!" },
                    {"code": 203, "icon": "error", "caption": "ERROR: Backup path has no free space!" },
                    {"code": 211, "icon": "error", "caption": "ERROR: Instance not exist!" },
                    {"code": 212, "icon": "error", "caption": "ERROR: When notification is activated, an instance must be selected!" },
                    {"code": 222, "icon": "error", "caption": "ERROR: CCU unreachable!" }
                ]
            }';
    }

    public function MessageSink($TimeStamp, $SenderID, $Message, $Data)
    {
        if ($Message === IPS_KERNELSTARTED) {
            $this->ApplyChanges();
        }
        return true;
    }

    protected function CURL($URL, $DATA, $DOWNLOAD)
    {
        $CCUAddress = $this->ReadPropertyString('CCUAddress');
        if ($this->ReadPropertyBoolean('SSL')) {
            $ch = curl_init('https://' . $CCUAddress . $URL);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        } else {
            $ch = curl_init('http://' . $CCUAddress . $URL);
        }
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        if ($DOWNLOAD === true) {
            ini_set('set_time_limit', 240);            //php.ini Timeout erhöhen
            curl_setopt($ch, CURLOPT_TIMEOUT, 240);    //curl Timeout erhöhen
            $saveFile = $this->ReadAttributeString('BackupFile');
            $fp = fopen($saveFile, 'wb+'); //File handler
            curl_setopt($ch, CURLOPT_FILE, $fp);
        } else {
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $DATA);
        }

        $result = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $rURL = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
        if ($DOWNLOAD === true) {
            fclose($fp);
        }
        if ($http_code >= 400 || $result === false) {
            $this->myDebug(__FUNCTION__ . '()', 'CCU unreachable: ' . $http_code);
            $this->SetStatus(222);
            return false;
        }
        $this->myDebug(__FUNCTION__ . '(url)', $rURL);
        $this->myDebug(__FUNCTION__ . '(http result)', $http_code);
        $this->myDebug(__FUNCTION__ . '(return)', $result);

        //echo $rURL;

        return $result;
    }

    private function BackupFile()
    {
        $Timestamp = date('Ymd_His');
        $CCUAddress = $this->ReadPropertyString('CCUAddress');
        $BNAME = str_replace('.', '_', $CCUAddress);
        $BPATH = $this->add_ending_slash($this->ReadPropertyString('BackupPath'));
        $BackupFile = $BPATH . 'CCU_' . $BNAME . '_' . $Timestamp . '.sbk';
        $this->WriteAttributeString('BackupFile', $BackupFile);
        return $BackupFile;
    }

    private function Check_BackupPath()
    {
        $SizeCheck = $this->ReadPropertyBoolean('SizeCheck');
        $Path = $this->ReadPropertyString('BackupPath');
        $File = $this->BackupFile();
        if ($Path === '') {
            return 201;
        }
        if (!is_dir($Path)) {
            return 202;
        }
        if (!$fp = @fopen($File, 'wb+')) {
            return 203;
        }
        unlink($File);
        //30mb free?
        if (($SizeCheck === true) && (!empty($Path) && disk_free_space($Path) < 30000000)) {
            $this->myDebug(__FUNCTION__ . '()', 'no free space on backup path!');
            return 204;
        }
        $this->myDebug(__FUNCTION__ . '()', 'ok: '.$File);
        return 102;
    }

    private function Check_Login()
    {
        $this->myDebug(__FUNCTION__ . '()', 'Login');
        $DATA = json_encode(array(
            'method' => 'Session.login',
            'params' => array(
                'username' => $this->ReadPropertyString('Username'),
                'password' => $this->ReadPropertyString('Password')
            )
        ), JSON_THROW_ON_ERROR, 512);

        $result = $this->CURL('/api/homematic.cgi', $DATA, false);
        $result = json_decode($result, true);
        //echo var_dump($result);

        if (is_array($result) AND ($result['result'] !== NULL)) {
            $this->WriteAttributeString('SessionID', $result['result']);
            return true;
        }
        return false;
    }

    private function Check_Logoff()
    {
        $this->myDebug(__FUNCTION__ . '()', 'Logoff');
        $DATA = json_encode(array(
            'method' => 'Session.logout',
            'params' => array(
                '_session_id_' => $this->ReadAttributeString('SessionID')
            )
        ), JSON_THROW_ON_ERROR, 512);

        $result = $this->CURL('/api/homematic.cgi', $DATA, false);
        $result = json_decode($result, true);
        //echo var_dump($result);

        if ($result['result'] === true && $result['error'] === null) {
            $this->WriteAttributeString('SessionID', '');
            return true;
        }
        return false;
    }

    private function CheckInstance()
    {
        $this->myDebug(__FUNCTION__ . '()', 'Wait for true..');
        if ($this->ReadPropertyBoolean('SmtpMsgEnabled') && $this->ReadPropertyInteger('SmtpInstanceID') === 0) {
            $this->SetStatus(212);
            return false;
        }
        if (@IPS_InstanceExists($this->ReadPropertyBoolean('SmtpInstanzID') === false)) {
            $this->myDebug(__FUNCTION__ . '-' . __FUNCTION__, $this->Translate('ERROR: SMTP instance not exist!'));
            $this->SetStatus(211);
            return false;
        }
        if (@IPS_InstanceExists($this->ReadPropertyBoolean('SmtpVarEnabled') === false)) {
            $this->myDebug(__FUNCTION__ . '-' . __FUNCTION__, $this->Translate('ERROR: SMTP bool variable instance not exist!'));
            $this->SetStatus(211);
            return false;
        }
        $this->myDebug(__FUNCTION__ . '()', 'True');
        return true;
    }

    private function add_ending_slash($path)
    {

        $slash_type = (strpos($path, '\\') === 0) ? 'win' : 'unix';

        $last_char = substr($path, strlen($path) - 1, 1);

        if ($last_char !== '/' && $last_char !== '\\') {
            // no slash:
            $path .= ($slash_type === 'win') ? '\\' : '/';
        }

        return $path;
    }

    private function DeleteOldBackups()
    {
        $now = time();
        $BackupDays = $this->ReadPropertyInteger('BackupDays');
        $BackupSecs = $BackupDays * 86400;
        $this->myDebug(__FUNCTION__ . '()', 'older ' . $BackupDays . ' days');
        $Path = $this->ReadPropertyString('BackupPath');
        $Pattern = $this->add_ending_slash($Path) . '*.sbk';
        foreach (glob($Pattern) as $f) {
            if (is_file($f) && ($now - filemtime($f) > $BackupSecs)) {
                $this->myDebug(__FUNCTION__ . '()', $f);
                unlink($f);
            }
        }
    }

    private function Fill_Variables($ValSTATE, $ValLAST)
    {
        $STATE = @$this->GetIDForIdent('STATE');
        if ($STATE) {
            SetValue($STATE, $ValSTATE);
        }
        $LAST = @$this->GetIDForIdent('LAST');
        if ($LAST) {
            SetValue($LAST, $ValLAST);
        }
    }

    private function myDebug($Message, $DATA)
    {
        if ($this->ReadPropertyBoolean('Debug')) {
            $this->SendDebug($Message, $DATA, 0);
        }
    }

    private function SetMyTimerInterval()
    {
        $days = $this->ReadPropertyInteger('BackupIntervall');

        if ($days === 0){
            $this->SetTimerInterval('CCU_Backup', 0);
        } else {
            $json = json_decode($this->ReadPropertyString('BackupTime'), true);

            $Now = new DateTime();
            $Target = new DateTime();
            $Target->modify($days . ' day');
            $Target->setTime($json['hour'], $json['minute'], $json['second']);
            $Diff = $Target->getTimestamp() - $Now->getTimestamp();
            $Interval = $Diff * 1000;

            $this->SetTimerInterval('CCU_Backup', $Interval);
        }
    }

    public function ReadableSize(float $size)
    {
        $base = log($size) / log(1024);
        $suffix = array('B', 'KB', 'MB', 'GB', 'TB');
        $f_base = floor($base);
        return round(1024 ** ($base - floor($base)), 1) . $suffix[$f_base];
    }

    private function ModulInfo()
    {
        return IPS_GetLibrary('{A7208307-8D4E-66B8-C352-704E24B137FC}');
    }

    private function SendMessages($State, $MsgTitle, $MsgText)
    {
        if ($this->ReadPropertyBoolean('SmtpMsgEnabled') === true) {
            $SmtpInstanzID = $this->ReadPropertyInteger('SmtpInstanceID');
            $SmtpVarEnabled = $this->ReadPropertyInteger('SmtpVarEnabled');
            if (($SmtpVarEnabled === 0) || (($SmtpVarEnabled > 0) && (GetValue($SmtpVarEnabled) === true))) {
                if (($State === false) || ($State === true && $this->ReadPropertyBoolean('SmtpMsgOnlyError') === false)) {
                    $this->myDebug(__FUNCTION__ . '()', 'Send message to SMTP instance');
                    SMTP_SendMail($SmtpInstanzID, $MsgTitle, $MsgText . "\n");
                }
            }
        }
    }
}