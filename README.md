# CCUBackup

[![Version](https://img.shields.io/badge/Symcon-PHPModul-red.svg)](https://www.symcon.de/)
[![Version](https://img.shields.io/badge/Modul%20Version-1.13%20Build:240502-blue.svg)]()
[![Version](https://img.shields.io/badge/Symcon%20Version->=%205.3-green.svg)]()
[![Version](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-orange.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de)
[![Version](https://img.shields.io/badge/Spenden-PayPal-yellow.svg?style=flat-square)](https://www.paypal.com/paypalme2/Simon6714?locale.x=de_DE)

---

Dieses Symcon Modul erstellt ein Backup der Homematic CCU.  
Beitrag im Forum:  
<https://www.symcon.de/forum/threads/43032-Modul-Homematic-CCU-Backup?p=419597#post419597> 

## 📝 Inhaltsverzeichnis

- [CCUBackup](#ccubackup)
  - [📝 Inhaltsverzeichnis](#-inhaltsverzeichnis)
  - [🧐 Funktionsumfang <a name = "Funktionsumfang"></a>](#-funktionsumfang-)
  - [🏁 Systemanforderungen  <a name = "Systemanforderungen"></a>](#-systemanforderungen--)
  - [🔧 Installation <a name = "Installation"></a>](#-installation-)
  - [✍ Autoren <a name = "Autoren"></a>](#-autoren-)
  - [📄 Lizenz <a name = "Lizenz"></a>](#-lizenz-)
  - [👓 Screenshots <a name = "Screenshots"></a>](#-screenshots-)
  - [🎉FAQ <a name = "FAQ"></a>](#faq-)

## 🧐 Funktionsumfang <a name = "Funktionsumfang"></a>

- Modul ist unabhängig zu anderen (Homematic) Modulen
- Manuelles Backup der CCU
- Automatisches Backup der CCU (Tage-Intervall und Uhrzeit einstellbar)
- RaspberryMatic, piVCCU3 (laut Benutzermeldungen im Forum erfolgreich)
- Auswahl des Backup Pfades (siehe FAQ)
- SSL Kommunikation (Optional)
- Backups (älter als X Tage) automatisch löschen
- Mail-Benachrichtigung (Immer oder nur im Fehlerfall)

## 🏁 Systemanforderungen  <a name = "Systemanforderungen"></a>

- IP-Symcon ab Version 5.3 (keine Legacy Console Unterstützung!)
- Homematic CCU

## 🔧 Installation <a name = "Installation"></a>

Das Modul in IP-Symcon einbinden:

1. In der IP-Symcon Console unter "Kern Instanzen" die "Module Control" Instanz öffnen
2. Auf "Hinzufügen" klicken, hier ist die Modul GIT-URL einzutragen:  
   <https://gitlab.com/SG-IPS-Module/Public/CCUBackup.git>
3. Nun kann im IPS-Objektbaum (an beliebiger Stelle) eine neue Modul-Instanz hinzufügt werden

## ✍ Autoren <a name = "Autoren"></a>

- [@Simon6714](https://github.com/Simon6714) - Idee und Umsetzung
- [@Bayaro](https://gitlab.com/Bayaro) - Codechecker

## 📄 Lizenz <a name = "Lizenz"></a>

Dieses Projekt unterliegt der [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de) Lizenz, und ist für die nicht kommerzielle Nutzung kostenlos.  
Schenkungen zur Unterstützung werden hier angenommen.  

[![](https://www.paypalobjects.com/de_DE/DE/i/btn/btn_donate_LG.gif)](https://www.paypal.me/Simon6714?locale.x=de_DE)

## 👓 Screenshots <a name = "Screenshots"></a>

WebFront:  
![Screenshot WebFront](imgs/CCUBackup1.png)

Konfigurationsformular:  
![Screenshot Formular](imgs/CCUBackup2.png)

## 🎉FAQ <a name = "FAQ"></a>

- Muss ich vor dem "manuellen" Backup an der CCU anmelden?  
  Nein. Die Buttons Anmelden, Anmeldestatus, Abmelden dienen lediglich zum Testen des Login (ob eine Session-ID von der CCU zu bekommen ist). 
- CCU ohne Anmeldung (Automatisches Anmelden aktiv)  
  Eine Anmeldung bzw. Backup ohne Benutzername und Kennwort wird vom Modul nicht unterstützt. 
  Du kannst für Symcon auf der CCU unter Einstellungen/Benutzerverwaltung einen extra Benutzer für das Backup erstellen. 
  Beachte das die Berechtigungsstufe: Administrator benötigt wird. 
- Unterstützt das Modul HTTPS?  
  Ja, es muss jedoch zwingend ein Zertifikat auf der CCU installiert sein!  
- Wie lange wartet das Modul auf die Auslieferung des Backups der CCU (Stichwort: max_execution_time)?  
  Die maximale Ausführungszeit ist auf 4 Minuten beschränkt.  
- Backup Pfad, speichern im Netzwerk?  
  Der Backup Pfad ist ein lokales Verzeichnis, jedoch kann dieser natürlich gemounted sein. Linux: CIFS, Windows: Netzlaufwerk.  
- Freien Speicherplatz prüfen?  
  Das Modul überprüft ob 30Mb freier Speicherplatz im Backup Pfad verfügbar ist.  
  Auf Mini-PCs kann diese Berechnung jedoch (durch Speicherbeschränkung) fehlschlagen.  
  Diese Überprüfung kann deaktiviert werden.   
  

